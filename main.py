import math
import os
from tkinter import *

import requests
from PIL import Image, ImageTk

EVENT_STATE = 393224  # 393216  # 393224
MAX_DISTANCE = 50  # metre
CHANGE_POS_WITH_CLICK = True


def lonlat_distance(a, b):
    degree_to_meters_factor = 111 * 1000  # 111 километров в метрах
    a_lat, a_lon = a
    b_lat, b_lon = b

    # Берем среднюю по широте точку и считаем коэффициент для нее.
    radians_lattitude = math.radians((a_lat + b_lat) / 2.)
    lat_lon_factor = math.cos(radians_lattitude)

    # Вычисляем смещения в метрах по вертикали и горизонтали.
    dx = abs(a_lon - b_lon) * degree_to_meters_factor * lat_lon_factor
    dy = abs(a_lat - b_lat) * degree_to_meters_factor

    # Вычисляем расстояние между точками.
    distance = math.sqrt(dx * dx + dy * dy)

    return distance


def get_map(lat, lon, spn, map_type, point, map_file='map.png'):
    try:
        api_server = "http://static-maps.yandex.ru/1.x/"

        params = {
            "ll": f'{lon},{lat}',
            "spn": f'{spn},{spn}',
            "l": map_type
        }
        if point[0] is not None and point[1] is not None:
            params["pt"] = f'{point[1]},{point[0]},pm2rdl'

        response = requests.get(api_server, params=params)

        if not response:
            print("Ошибка выполнения запроса:")
            print("Http статус:", response.status_code, "(", response.reason, ")")
            return
    except:
        print("Запрос не удалось выполнить. Проверьте наличие сети Интернет.")
        return

    try:
        with open(map_file, "wb") as file:
            file.write(response.content)
    except IOError as ex:
        print("Ошибка записи временного файла:", ex)
        return


def geocode(name):
    geo_coder_api_server = "http://geocode-maps.yandex.ru/1.x/"
    geo_coder_params = {"geocode": name, "format": "json"}
    res = requests.get(geo_coder_api_server, params=geo_coder_params).json()

    try:
        object_raw = res["response"]["GeoObjectCollection"]["featureMember"][0]['GeoObject']
        coordinates = object_raw["Point"]["pos"]
        address = object_raw['metaDataProperty']['GeocoderMetaData']['AddressDetails']['Country']['AddressLine']

        try:
            post_code = object_raw['metaDataProperty']['GeocoderMetaData']['Address']['postal_code']
        except KeyError:
            post_code = 'не найден!'

    except IndexError:
        return ''

    except KeyError:
        return ''

    longitude, lattitude = coordinates.split(' ')
    return lattitude, longitude, address, post_code


def reverse_geocoder(lat, long):
    lat = max(min(85.0, float(lat)), -85.0)
    long = max(min(179.0, float(long)), -179.0)

    geo_coder_api_server = "http://geocode-maps.yandex.ru/1.x/"
    geo_coder_params = {"geocode": f"{long},{lat}", "format": "json"}
    response = requests.get(geo_coder_api_server, params=geo_coder_params).json()

    try:
        object_raw = response["response"]["GeoObjectCollection"]["featureMember"][0]['GeoObject']
        coordinates = object_raw["Point"]["pos"]
        address = object_raw['metaDataProperty']['GeocoderMetaData']['AddressDetails']['Country']['AddressLine']

        try:
            post_code = object_raw['metaDataProperty']['GeocoderMetaData']['Address']['postal_code']
        except KeyError:
            post_code = 'не найден!'

    except IndexError:
        return ''

    except KeyError:
        return ''

    longitude, lattitude = coordinates.split()
    return lattitude, longitude, address, post_code


def search_org(lat, long):
    search_api_server = "https://search-maps.yandex.ru/v1/"
    api_key = "3c4a592e-c4c0-4949-85d1-97291c87825c"

    address_ll = f"{long},{lat}"

    search_params = {
        "apikey": api_key,
        "lang": "ru_RU",
        "ll": address_ll,
        "type": "geo,biz",
        "spn": "0.001,0.001"
    }

    response = requests.get(search_api_server, params=search_params)

    json_response = response.json()

    if 'features' not in json_response:
        return False

    data = {}

    for i in json_response["features"]:
        try:
            organization = i["properties"]["CompanyMetaData"]

            org_name = organization["name"]
            org_address = organization["address"]
            org_timetable = organization['Hours']['text']

            point = i["geometry"]["coordinates"]
            org_point = (point[1], point[0])

            dist = lonlat_distance(org_point, (lat, long))

            data[dist] = (org_name, org_address, org_timetable, org_point, dist)

        except KeyError:
            pass

    if data:
        return data[min(data)]
    return False


class Window(Frame):
    def __init__(self, master=None):
        Frame.__init__(self, master)

        self.master = master
        self.point = (54.509316, 36.273797)
        self.lat = DoubleVar(value=54.509316)
        self.long = DoubleVar(value=36.273797)
        self.address = StringVar(value='')
        self.mode = StringVar(value='map')
        self.scale = DoubleVar(value=0.001)
        self.scale_var = IntVar(value=0)
        self.auto_loading = BooleanVar(value=True)
        self.on_post_code = BooleanVar(value=True)
        get_map(54.509316, 36.273797, 0.001, 'map', (54.509316, 36.273797))
        self.init_window()

    # Creation of init_window
    def init_window(self):
        # changing the title
        self.master.title("Большая задача по Maps API (made by Alex Sizov, Georgy Komarov, Ruslan Stepanov)")

        self.pack(fill=BOTH, expand=1)

        # creating labels
        lat_label = Label(self, text='Широта')
        lat_label.place(x=0, y=60)

        long_label = Label(self, text='Долгота')
        long_label.place(x=0, y=95)

        scale_label = Label(self, text='Масштаб (spn)')
        scale_label.place(x=0, y=130)

        search_label = Label(self, text='Поиск')
        search_label.place(x=0, y=250)

        # creating buttons
        quit_button = Button(self, text="Выход", command=self.client_exit)
        quit_button.place(x=0, y=0)

        new_map_button = Button(self, text="Загрузить карту", command=self.new_map)
        new_map_button.place(x=0, y=30)

        load_address_button = Button(self, text="Найти", command=self.load_address)
        load_address_button.place(x=0, y=285)

        delete_search_point = Button(self, text="Удалить метку", command=self.del_search_point)
        delete_search_point.place(x=0, y=375)

        # creating text boxes
        self.latitude_textbox = Entry(self, textvariable=self.lat)
        self.latitude_textbox.place(x=0, y=75)

        self.longitude_textbox = Entry(self, textvariable=self.long)
        self.longitude_textbox.place(x=0, y=110)

        self.scale_textbox = Entry(self, textvariable=self.scale)
        self.scale_textbox.place(x=0, y=145)

        self.address_textbox = Entry(self, textvariable=self.address)
        self.address_textbox.place(x=0, y=265)

        # creating radio buttons
        self.map_mode_button = Radiobutton(
            self, cnf={'variable': self.mode, 'value': 'map', 'text': 'Карта'},
            command=self.check_loading
        )
        self.map_mode_button.place(x=0, y=170)

        self.sattelite_mode_button = Radiobutton(
            self, cnf={'variable': self.mode, 'value': 'sat', 'text': 'Спутник'},
            command=self.check_loading
        )
        self.sattelite_mode_button.place(x=0, y=195)

        self.hybrid_mode_button = Radiobutton(
            self, cnf={'variable': self.mode, 'value': 'skl', 'text': 'Гибрид'},
            command=self.check_loading
        )
        self.hybrid_mode_button.place(x=0, y=220)

        # creating checkboxes
        self.auto_loading_checkbox = Checkbutton(self, cnf={'variable': self.auto_loading,
                                                            'text': 'Автоматическая загрузка карты'})
        self.auto_loading_checkbox.place(x=0, y=325)

        self.post_code_checkbox = Checkbutton(self,
                                              cnf={'variable': self.on_post_code, 'text': 'Показывать почтовый индекс'},
                                              command=self.load_address)
        self.post_code_checkbox.place(x=0, y=345)

        # setting map
        load = Image.open("map.png")
        render = ImageTk.PhotoImage(load)

        self.img = Label(self, image=render)
        self.img.image = render
        self.img.place(x=200, y=0)

        # generating slider variables

        self.scale_list = [0.001, 0.002, 0.004, 0.008, 0.016, 0.032, 0.064, 0.128, 0.256, 0.512, 1.024, 2.048, 4.096,
                           8.192, 16.384, 32.768]

        # creating slider

        self.scale_slider = Scale(root, variable=self.scale_var, showvalue=0, length=450, command=self.scroll_update,
                                  from_=0, to=len(self.scale_list) - 1)
        self.scale_slider.place(x=803, y=0)

        # binding event listeners
        self.master.bind("<KeyPress>", self.key_pressed)
        # Windows OS
        self.master.bind("<MouseWheel>", self.mouse_wheel)
        # Linux OS
        self.master.bind("<Button-4>", self.mouse_wheel)
        self.master.bind("<Button-5>", self.mouse_wheel)
        self.img.bind("<Button-1>", self.left_mouse_click)
        self.img.bind("<Button-3>", self.right_mouse_click)
        self.scale_slider.bind('<ButtonRelease-1>', self.map_update)

    def client_exit(self):
        try:
            os.remove('map.png')
            os.remove('map2.png')
        except Exception:
            pass
        exit()

    def new_map(self):
        lat = self.lat.get()
        long = self.long.get()
        mode = self.mode.get()
        scale = self.scale.get()

        # checking coords to be correct
        lat = max(min(85.0, float(lat)), -85.0)
        long = max(min(179.0, float(long)), -179.0)
        self.lat.set(lat)
        self.long.set(long)

        # loading map
        get_map(lat, long, scale, mode, self.point)
        load = Image.open("map.png")
        render = ImageTk.PhotoImage(load)
        if mode == 'skl':
            get_map(lat, long, scale, 'sat', self.point, map_file="map2.png")
            sat = self.RBGAImage("map2.png")
            hybrid = self.RBGAImage("map.png")

            sat.paste(hybrid, (0, 0), hybrid)

            skl = ImageTk.PhotoImage(sat)
            self.img = Label(self, image=skl)
            self.img.image = skl
            self.img.place(x=200, y=0)
        else:
            self.img = Label(self, image=render)
            self.img.image = render
            self.img.place(x=200, y=0)

        self.img.bind("<Button-1>", self.left_mouse_click)
        self.img.bind("<Button-3>", self.right_mouse_click)

    def load_address(self, lat=None, long=None):
        try:
            if lat is not None and long is not None:
                lat, long, address, post_code = reverse_geocoder(lat, long)
            else:
                lat, long, address, post_code = geocode(self.address.get())
        except Exception:
            label = Label(self, text='Что-то пошло не так!' + '\t' * 10)
            label.place(x=0, y=460)
            self.master.geometry("824x480")
            return

        self.lat.set(value=lat)
        self.long.set(value=long)
        self.address.set(address)

        label_text = f'Адрес: {address}\t\t\t{f"Почтовый индекс: {post_code}" if self.on_post_code.get() else ""}'
        label = Label(self, text=label_text + '\t' * 10)  # костыль \t на переотрисовку
        label.place(x=0, y=460)
        self.master.geometry("824x480")

        self.point = (lat, long)
        self.new_map()

    def scroll_update(self, *args):
        new_value = self.scale_list[self.scale_var.get()]  # DoubleVar.get() tkinter error fix
        self.scale.set(new_value)
        self.scale_textbox.delete(0, len(self.scale_textbox.get()) + 1)
        self.scale_textbox.insert(0, new_value)

    def map_update(self, *args):
        self.scroll_update()
        self.check_loading()

    def check_loading(self):
        if self.auto_loading.get():
            self.new_map()

    def del_search_point(self):
        self.point = (None, None)
        self.master.geometry("824x450")
        self.new_map()

    # event listener
    def key_pressed(self, event):
        if event.keysym == 'Prior':  # PageUp
            self.scale_var.set(self.scale_var.get() + 1)

        elif event.keysym == 'Next':  # PageDown
            self.scale_var.set(self.scale_var.get() - 1)

        elif event.keysym == 'Up' and event.state == 393216:
            self.scale_var.set(value=int(self.scale_var.get()) - 1)

        elif event.keysym == 'Down' and event.state == 393216:
            self.scale_var.set(value=int(self.scale_var.get()) + 1)

        elif event.keysym == 'Up':  # Arrow up
            self.lat.set(value=float(self.lat.get()) + self.scale.get() * 0.6)

        elif event.keysym == 'Down':  # Arrow down
            self.lat.set(value=float(self.lat.get()) - self.scale.get() * 0.6)

        elif event.keysym == 'Left':  # Arrow left
            self.long.set(value=float(self.long.get()) - self.scale.get())

        elif event.keysym == 'Right':  # Arrow right
            self.long.set(value=float(self.long.get()) + self.scale.get())

        elif event.keysym == 'Return':  # Return
            self.load_address()
            self.new_map()

        else:
            return

        self.map_update()

    # event listener
    def mouse_wheel(self, event):
        self.scale_var.set(value=int(self.scale_var.get()) - (event.delta // 120) if event.delta != 0 else int(
            self.scale_var.get()) - (-1 if event.num == 5 else 1))  # Condition to work in Linux properly
        self.map_update()

    def left_mouse_click(self, event):
        lat, long = self.get_coords_from_click(event.x, event.y)

        if CHANGE_POS_WITH_CLICK:
            self.lat.set(value=lat)
            self.long.set(value=long)
            self.new_map()

        try:
            self.load_address(lat, long)
        except Exception:
            label = Label(self, text='Что-то пошло не так!' + '\t' * 10)
            label.place(x=0, y=460)
            self.master.geometry("824x480")
            return

    def right_mouse_click(self, event):
        lat, long = self.get_coords_from_click(event.x, event.y)

        if CHANGE_POS_WITH_CLICK:
            self.lat.set(value=lat)
            self.long.set(value=long)
            self.new_map()

        try:
            name, address, time, point, dist = search_org(lat, long)
        except Exception:
            label = Label(self, text='Что-то пошло не так!' + '\t' * 10)
            label.place(x=0, y=460)
            self.master.geometry("824x480")
            return

        self.lat.set(point[0])
        self.long.set(point[1])
        self.address.set(address)

        if dist > MAX_DISTANCE:
            label = Label(self, text=f'Организации в радиусе {MAX_DISTANCE} метров отсутствуют!' + '\t' * 10)
            label.place(x=0, y=460)
            self.master.geometry("824x480")
            return

        for label_num, label_text in enumerate([name, address, time, point, dist]):
            bottom_labels = {0: {'name': 'Адрес', 'value': address},
                             1: {'name': 'Организация', 'value': name},
                             2: {'name': 'Время работы', 'value': time},
                             3: {'name': 'Координаты', 'value': f"{point[0]}, {point[1]}"}}
            
            if label_num not in bottom_labels:
                continue

            name_ = bottom_labels[label_num]['name']
            value = bottom_labels[label_num]['value']

            label = Label(self, text='{}: {}'.format(name_, value) + '\t' * 10)  # костыль \t на переотрисовку
            label.place(x=0, y=460 + label_num * 20)

        self.master.geometry("824x540")

        self.point = point
        self.address.set(address)
        self.new_map()

    def get_coords_from_click(self, x, y):
        x = (x - 300)
        y = -(y - 225)

        scale = self.scale.get()

        lat = self.lat.get() + scale * (y / 310)
        long = self.long.get() + scale * (x / 185)

        return lat, long

    @staticmethod
    def RBGAImage(path):
        return Image.open(path).convert("RGBA")


if __name__ == '__main__':
    root = Tk()

    width, height = 824, 450
    root.geometry(f"{width}x{height}")
    x = (root.winfo_screenwidth() - root.winfo_reqwidth()) / 2 - width // 2 + 100
    y = (root.winfo_screenheight() - root.winfo_reqheight()) / 2 - height // 2 + 100

    root.wm_geometry("+%d+%d" % (x, y))

    root.resizable(0, 0)

    app = Window(root)
    mainloop()

    try:
        os.remove('map.png')
        os.remove('map2.png')
    except OSError:
        pass
